<meta name="msapplication-square70x70logo" content="<?php bloginfo( 'template_directory' ); ?>/favicons/windows-tile-70x70.png">
<meta name="msapplication-square150x150logo" content="<?php bloginfo( 'template_directory' ); ?>/favicons/windows-tile-150x150.png">
<meta name="msapplication-square310x310logo" content="<?php bloginfo( 'template_directory' ); ?>/favicons/windows-tile-310x310.png">
<meta name="msapplication-TileImage" content="<?php bloginfo( 'template_directory' ); ?>/favicons/windows-tile-144x144.png">
<meta name="msapplication-TileColor" content="#FDE5C5">
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php bloginfo( 'template_directory' ); ?>/favicons/apple-touch-icon-152x152-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php bloginfo( 'template_directory' ); ?>/favicons/apple-touch-icon-120x120-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php bloginfo( 'template_directory' ); ?>/favicons/apple-touch-icon-76x76-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="/favicons/apple-touch-icon-60x60-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php bloginfo( 'template_directory' ); ?>/favicons/apple-touch-icon-144x144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php bloginfo( 'template_directory' ); ?>/favicons/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php bloginfo( 'template_directory' ); ?>/favicons/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo( 'template_directory' ); ?>/favicons/apple-touch-icon.png">
<link rel="icon" sizes="228x228" href="<?php bloginfo( 'template_directory' ); ?>/favicons/coast-icon-228x228.png">
<link rel="shortcut icon" href="<?php bloginfo( 'template_directory' ); ?>/favicons/favicon.ico">
<link rel="icon" type="image/png" sizes="64x64" href="<?php bloginfo( 'template_directory' ); ?>/favicons/favicon.png">
