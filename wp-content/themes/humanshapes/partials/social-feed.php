<script id="feeds-template" type="text/x-handlebars-template">
  <div class="post-content">
    {{#if image}}
      <a href="{{link}}" title="View Post">
        <img src="{{image}}">
      </a>
    {{/if}}
    <div class="post-meta">
      <a href="{{link}}" class="post-meta-social icon-{{source}}-dark" title="View Post">
        <h1>Via {{source}}</h1>
        <p class="post-date">{{time}}</p>
      </a>
    </div>
    <p>{{{content_linked}}}</p>
  </div>
</script>

<div class="connect js-feed"></div>
