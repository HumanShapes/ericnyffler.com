<form class="contact js-contact" accept-charset="UTF-8" action="https://formkeep.com/f/4fe2a4fb5dad" method="POST">
  <input type="hidden" name="utf8" value="✓">
  <h1 class="contact-text icon-phone">Help Desk &amp; Service Center</h1>
  <div class="contact-container">
    <div class="contact-container-inputs">
      <input type="text" name="name" placeholder="Name..." tabindex=1>
      <input type="email" name="email" placeholder="Email Address..." tabindex=2 required>
      <input class="submit" type="submit" value="Submit" tabindex=4>
    </div>
    <textarea name="message" placeholder="Message..." tabindex=3 required></textarea>
    <p class="contact-loading">Sending...</p>
    <p class="contact-error">Something went wrong. Please try again or email <a href="mailto:eric@ericnyffeler.com">eric@ericnyffeler.com</a>.</p>
    <div class="contact-success icon-thanks"></div>
  </div>
</form>
