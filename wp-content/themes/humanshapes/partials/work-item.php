<?php
global $post;
$postID = $post->ID;
$hs_color = get_post_meta($postID, '_hs_color', true);
$hs_new = get_post_meta($post->ID, '_hs_new', true);
?>
<article class="work-item">
  <?php
    $image_id = get_post_thumbnail_id();
    $image_low_res = wp_get_attachment_image_src($image_id,'work_lowres', true);
    $image_post = wp_get_attachment_image_src($image_id,'work', true);
    $image_post_2x = wp_get_attachment_image_src($image_id,'work_2x', true);
  ?>
  <!--[if lte IE 9 ]>
  <img
    src="<?php echo $image_post[0]; ?>"
  >
  <![endif]-->
  <!--[if gt IE 9]><!-->
  <img
    src="<?php echo $image_low_res[0]; ?>"
    data-src="<?php echo $image_post[0]; ?>"
    data-src-retina="<?php echo $image_post_2x[0]; ?>"
  >
  <!--<![endif]-->
  <?php if (is_front_page() || isset($_GET["featured"])) { ?>
    <a class="work-item-overlay" href="<?php echo add_query_arg('featured', '', the_permalink()); ?>">
  <?php } else { ?>
    <a class="work-item-overlay" href="<?php the_permalink(); ?>">
  <?php } ?>
    <div class="work-item-overlay-bg" style="background-color: #<?php echo $hs_color ?>"></div>
    <div class="work-item-overlay-text">
      <div class="work-item-overlay-text-center">
        <?php if ($hs_new == "new") { ?>
          <h1 class="work-new icon-flag"><?php the_title(); ?></h1>
        <?php } else { ?>
          <h1><?php the_title(); ?></h1>
        <?php } ?>
        <p>
          <?php // Show tags unlinked
            $worktags = get_the_tags();
            if ($worktags) {
              $output = "";
              foreach($worktags as $tag) {
                $output .= $tag->name . ", ";
              }
              $output = substr($output , 0, -2);
              echo $output;
            }
          ?>
        </p>
      </div>
    </div>
  </a>
</article>
