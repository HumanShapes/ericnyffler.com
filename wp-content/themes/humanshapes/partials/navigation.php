<?php
  $previous_work = get_previous_posts_link( "< Newer Projects" );
  $next_work = get_next_posts_link( "Older Projects >", $featured_posts->max_num_pages );
  if (strlen($previous_work) > 0 || strlen($next_work) > 0) {
?>
  <div class="post-navigation-wrap">
    <div class="post-navigation">
      <div class="alignleft">
        <p class="post-back-alt"><?php echo $previous_work; ?></p>
      </div>
      <div class="alignright">
        <p class="post-next-alt"><?php echo $next_work; ?></p>
      </div>
    </div>
  </div>
<?php } ?>
