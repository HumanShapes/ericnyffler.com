<?php
global $post;
$tags = wp_get_post_tags($post->ID);
$tag_ids = array();
  foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
$args = array (
  "post_type" => array( "work" ),
  "tag__in" => $tag_ids,
  "post__not_in" => array($post->ID),
  "orderby" => "date",
  "order" => "DESC",
  "posts_per_page" => 8
);
$hs_work = new WP_Query( $args );
if ($hs_work->have_posts()) : ?>
  <section class="work-wrap">
    <div class="work">
      <div class="work-list js-masonry-related">
        <?php while ($hs_work->have_posts()) : $hs_work->the_post(); ?>
          <?php get_template_part("partials/work", "item"); ?>
        <?php endwhile; ?>
      </div>
    </div>
  </section>
<?php endif; ?>
