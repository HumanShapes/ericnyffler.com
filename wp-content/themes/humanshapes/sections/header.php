<header class="header-wrap">
  <div class="header">
    <a class="header-logo" href="/" title="">Home</a>
    <nav class="header-nav">
      <p><?php echo get_bloginfo("name") . '<span> / </span><br/>' . get_bloginfo("description");  ?></p>
      <?php
        // if (is_archive()) {
        //   $wp_query = NULL;
        //   $wp_query = new WP_Query(array("post_type" => "post"));
        // }
      ?>
      <?php wp_nav_menu( array( "theme_location" => "main-nav", "container" => "", "menu_class" => "header-nav-list" ) ); ?>
      <?php wp_reset_query(); ?>
    </nav>
  </div>
</header>
