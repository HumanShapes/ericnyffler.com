<footer class="footer-wrap">
  <div class="footer">
    <?php get_template_part("partials/contact"); ?>
    <div class="footer-info">
      <div class="footer-half">
        <h1>Stalking Information</h1>
        <address>
          <span>Eric Nyffeler</span><br/>
          P.O Box 4339<br/>
          Omaha, NE 68104, US <em>of</em> A
        </address>
        <p><a href="eric@ericnyffeler.com" title="Email Me">eric@ericnyffeler.com</a></p>
      </div>
      <div class="footer-half">
        <ul class="footer-social">
          <li><a class="icon-instagram" href="https://instagram.com/ericnyffeler" title="">Instagram</a></li>
          <li><a class="icon-dribble" href="https://dribbble.com/doe_eyed" title="">Dribble</a></li>
          <li><a class="icon-facebook" href="https://www.facebook.com/doe.eyed.design" title="">Facebook</a></li>
          <li><a class="icon-twitter" href="https://twitter.com/ericnyffeler" title="">Twitter</a></li>
          <li><a class="icon-tumblr" href="http://ericnyffeler.tumblr.com" title="">Tumblr</a></li>
        </ul>
        <a class="footer-button" href="http://eepurl.com/bCskcX" target="_blank" title="Sign Up for Eric's Newsletter"><span class="icon-mail"></span>Join My Mailing List</a>
      </div>
    </div>
  </div>
</footer>
