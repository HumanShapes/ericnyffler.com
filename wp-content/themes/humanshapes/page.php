<?php get_header(); ?>

<section class="post-single-wrap">
  <div class="post-single">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="post-single-left">
        <div class="post-content">
          <?php the_post_thumbnail(); ?>
          <div class="post-meta">
            <h1><?php the_title(); ?></h1>
          </div>
          <?php the_content(); ?>
          <?php edit_post_link("Edit"); ?>
        </div>
      </div>
    <?php endwhile; endif; ?>
  </div>
</section>

<?php get_footer(); ?>
