<?php get_header(); ?>

<?php get_template_part("sections/work-tags"); ?>
<?php
global $query_string;
query_posts( $query_string . "&posts_per_page=-1" );
?>
<section class="work-wrap">
  <div class="work">
    <div class="work-list js-masonry">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php get_template_part("partials/work", "item"); ?>
      <?php endwhile; endif; ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>
