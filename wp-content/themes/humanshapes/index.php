<?php get_header(); ?>

<section class="post-list-wrap">
  <div class="post-list">
    <div class="post-half post-left js-masonry-paged">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="post-content-wrap">
          <div class="post-content">
            <?php
              $image_id = get_post_thumbnail_id();
              $image_low_res = wp_get_attachment_image_src($image_id,'post_lowres', true);
              $image_post = wp_get_attachment_image_src($image_id,'post', true);
              $image_post_2x = wp_get_attachment_image_src($image_id,'post_2x', true);
            ?>
            <!--[if lte IE 9 ]>
            <img
              src="<?php echo $image_post[0]; ?>"
            >
            <![endif]-->
            <!--[if gt IE 9]><!-->
            <img
              src="<?php echo $image_low_res[0]; ?>"
              data-src="<?php echo $image_post[0]; ?>"
              data-src-retina="<?php echo $image_post_2x[0]; ?>"
            >
            <!--<![endif]-->
            <div class="post-meta">
              <h1><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h1>
              <p class="post-date"><?php the_time("F jS, Y"); ?></p>
            </div>
            <?php the_content(); ?>
            <?php edit_post_link("Edit"); ?>
          </div>
        </div>
      <?php endwhile; endif; ?>
      <?php // Can merge with navigation as long as this only shows for index and paged index pages and won't affect the archive.php
        $previous_work = get_previous_posts_link( "< Newer Posts" );
        $next_work = get_next_posts_link( "Older Posts >", "" );
        if (strlen($previous_work) > 0 || strlen($next_work) > 0) {
      ?>
        <div class="post-navigation-wrap">
          <div class="post-navigation">
            <div class="alignleft">
              <p class="post-back-alt"><?php echo $previous_work; ?></p>
            </div>
            <div class="alignright">
              <p class="post-next-alt"><?php echo $next_work; ?></p>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
    <div class="post-half post-right">
      <?php get_template_part("partials/social", "feed"); ?>
    </div>
    <div class="post-navigation-wrap post-navigation-wrap-alt">
      <div class="post-navigation">
        <div class="alignleft">
          <p class="post-back-alt"><?php echo $previous_work; ?></p>
        </div>
        <div class="alignright">
          <p class="post-next-alt"><?php echo $next_work; ?></p>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
