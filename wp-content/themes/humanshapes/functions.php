<?php

// Allow pages to have excerpts for use in headers such as What We Do
add_action( "init", "my_add_excerpts_to_pages" );
function my_add_excerpts_to_pages() {
  add_post_type_support( "page", "excerpt" );
}

// Allow HTML descriptions in WordPress Menu
remove_filter( "nav_menu_description", "strip_tags" );
add_filter( "wp_setup_nav_menu_item", "cus_wp_setup_nav_menu_item" );
function cus_wp_setup_nav_menu_item( $menu_item ) {
  if (property_exists($menu_item, "post_content")) {
    $menu_item->description = apply_filters( "nav_menu_description", $menu_item->post_content );
  }
  return $menu_item;
}

// Hide that Admin Bar that kind of messes with the fixed header
add_filter("show_admin_bar", "__return_false");

// Custom Image Sizes
add_theme_support( "post-thumbnails", array( "post", "work", "page" ));
add_image_size( "work_lowres", 35 ); // Work Low-res
add_image_size( "work", 280 ); // Work
add_image_size( "work_2x", 560 ); // ^ Hi-Res
add_image_size( "square_large", 436, 436, true ); // Large teammate photos
add_image_size( "post_lowres", 35 ); // Full-width hero images for pages Low-res
add_image_size( "post", 550 ); // Full-width hero images for pages
add_image_size( "post_2x", 1100 ); // ^ Hi-res
add_image_size( "gallery_lowres", 35 ); // Gallery Low-res
add_image_size( "gallery", 875 ); // Gallery
add_image_size( "gallery_2x", 1750 ); // ^ Hi-res

// Wrap Auto-embeds in video-wrapper for responsiveness
add_filter("embed_oembed_html", "hs_embed_oembed_html", 99, 4);
function hs_embed_oembed_html($html, $url, $attr, $post_id) {
  return '<div class="video-wrapper">' . $html . '</div>';
}

// Add the menus
function register_my_menus() {
  register_nav_menus(
    array(
      "main-nav" => __( "Main Navigation" ),
      "tags-nav" => __( "Tag Navigation" )
    )
  );
}
add_action( "init", "register_my_menus" );

// Set default meta boxes
function hs_default_hidden_meta_boxes( $hidden, $screen ) {
  // Grab the current post type
  $post_type = $screen->post_type;

  if ( $post_type == "post" ) {
    // Define which meta boxes we wish to hide on 'post'
    // Uncommented lines will be unchecked for new users
    $hidden = array(
      "authordiv", // Author metabox
      //"categorydiv", // Categories metabox.
      "commentstatusdiv", // Comments status metabox (discussion)
      "commentsdiv", // Comments metabox
      //"formatdiv", // Formats metabox
      //"pageparentdiv", // Attributes metabox
      "postcustom", // Custom fields metabox
      //"postexcerpt", // Excerpt metabox
      //"postimagediv", // Featured image metabox
      "revisionsdiv", // Revisions metabox
      "slugdiv", // Slug metabox
      //"submitdiv", // Date, status, and update/save metabox
      //"tagsdiv-post_tag", // Tags metabox
      //"{$tax-name}div", // Hierarchical custom taxonomies metabox
      "trackbacksdiv" // Trackbacks metabox
    );

    return $hidden;
  } else if ( $post_type == "page" ) {
    // Define which meta boxes we wish to hide on "page"
    // Uncommented lines will be unchecked for new users
    $hidden = array(
      //"authordiv", // Author metabox
      //"categorydiv", // Categories metabox.
      "commentstatusdiv", // Comments status metabox (discussion)
      "commentsdiv", // Comments metabox
      //"formatdiv", // Formats metabox
      //"pageparentdiv", // Attributes metabox
      "postcustom", // Custom fields metabox
      //"postexcerpt", // Excerpt metabox
      //"postimagediv", // Featured image metabox
      "revisionsdiv", // Revisions metabox
      "slugdiv", // Slug metabox
      //"submitdiv", // Date, status, and update/save metabox
      //"tagsdiv-post_tag", // Tags metabox
      //"{$tax-name}div", // Hierarchical custom taxonomies metabox
      "trackbacksdiv" // Trackbacks metabox
    );

    return $hidden;
  } else if ( is_blog_admin() ) {
    // Define which meta boxes we wish to hide on "dashboard"
    // Uncommented lines will be unchecked for new users
    $hidden = array(
      "dashboard_right_now", // At a Glance
      "dashboard_activity", // Activity
      "dashboard_quick_press", // Quick Draft
      "dashboard_primary" // WordPress News
    );

    return $hidden;
  }

  // If we are not on a "post" or "page", pass the original defaults, as defined by WordPress
  return $hidden;
}

add_action( "default_hidden_meta_boxes", "hs_default_hidden_meta_boxes", 10, 2 );

add_filter("gform_init_scripts_footer", "init_scripts");
  function init_scripts() {
  return true;
}

// Get featured image URL
function hs_featured_image_url($id, $size) {
  if ( has_post_thumbnail($id) ) {
    $imageURL = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), $size );
    $imageURL = $imageURL[0];
    return $imageURL;
  } else {
    return false;
  }
}

// Add Navigation for Single Blog Posts and Work Pages
function hs_single_navigation($type) {
  next_post_link('<p class="post-back"><span class="icon-arrow-left">%link</span></p>', "Previous $type");
  previous_post_link('<p class="post-next"><span class="icon-arrow-right">%link</span></p>', "Next $type");
  echo '<div class="post-pagination">';
    next_post_link('<p class="post-back-alt">%link</p>', "< Previous $type");
    previous_post_link('<p class="post-next-alt">%link</p>', "Next $type >");
  echo '</div>';
}

// Add custom post type archive pages functionality
function namespace_add_custom_types( $query ) {
  if( ( is_category() || is_tag() ) && empty( $query->query_vars['suppress_filters'] ) ) {
    $query->set( 'post_type', array(
     'post', 'nav_menu_item', 'work'
		));
	  return $query;
	}
}
add_filter( 'pre_get_posts', 'namespace_add_custom_types' );

function  strip_shortcode_gallery( $content ) {
  preg_match_all( '/'. get_shortcode_regex() .'/s', $content, $matches, PREG_SET_ORDER );
  if ( ! empty( $matches ) ) {
    foreach ( $matches as $shortcode ) {
      if ( 'gallery' === $shortcode[2] ) {
        $pos = strpos( $content, $shortcode[0] );
        if ($pos !== false)
          return substr_replace( $content, '', $pos, strlen($shortcode[0]) );
      }
    }
  }
  return $content;
}
