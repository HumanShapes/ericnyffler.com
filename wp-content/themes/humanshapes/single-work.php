<?php get_header(); ?>

<section class="post-single-wrap">
  <div class="post-single">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <?php
      global $post;
      $postID = $post->ID;
      $hs_shop_url = get_post_meta($postID, "_hs_shop_url", true);
      $hs_spaced = get_post_meta($post->ID, '_hs_spaced', true);
      ?>
      <?php if ($hs_spaced == "spaced") { ?>
        <div class="post-gallery post-gallery--mobile post-gallery--spaced">
      <?php } else { ?>
      <div class="post-gallery post-gallery--mobile">
      <?php } ?>
        <div class="post-gallery-wrap">
          <?php
          $gallery = get_post_gallery();
          echo $gallery;
          ?>
        </div>
      </div>
      <div class="post-single-left js-sticky">
        <div class="post-content">
          <div class="post-meta">
            <h1><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h1>
            <p class="post-tags"><?php the_tags(""); ?></p>
          </div>
          <?php
          $content = strip_shortcode_gallery( get_the_content() );
          $content = str_replace( ']]>', ']]&gt;', apply_filters( 'the_content', $content ) );
          echo $content;
          ?>
          <?php edit_post_link("Edit"); ?>
        </div>
        <?php
        if ($hs_shop_url) {
          echo '<a class="post-callout icon-callout" href="' . $hs_shop_url .'" title="Available in the Webstore">Available in the Webstore</a>';
        }
        ?>
        <?php if (isset($_GET["featured"])) { ?>
          <div class="post-pagination">
            <p class="post-pagination-centered">
              <?php
                // paginate through featured posts by menu_order
                $smartNextPost = next_post_link_plus( array(
                  'order_by' => 'menu_order',
                  'order_2nd' => 'post_date',
                  'loop' => 1,
                  'format' => '< %link',
                  'link' => 'Previous Project',
                  'in_same_cat' => true,
                  'in_cats' => '8', // set to 7 for local database (otherwise 8)
                  'return' => 'href'
                )) . "?featured";
                $smartPreviousPost = previous_post_link_plus( array(
                  'order_by' => 'menu_order',
                  'order_2nd' => 'post_date',
                  'loop' => 1,
                  'format' => '%link >',
                  'link' => 'Next Project',
                  'in_same_cat' => true,
                  'in_cats' => '8', // set to 7 for local database (otherwise 8)
                  'return' => 'href'
                )) . "?featured";
                echo '<a href="'.$smartNextPost.'">< Previous Project</a> / <span><a href="'.$smartPreviousPost.'">Next Project ></a></span>';
              ?>
            </p>
          </div>
        <?php } ?>
      </div>
      <?php if ($hs_spaced == "spaced") { ?>
        <div class="post-gallery post-gallery--desktop post-gallery--spaced">
      <?php } else { ?>
      <div class="post-gallery post-gallery--desktop">
      <?php } ?>
        <div class="post-gallery-wrap">
          <?php echo $gallery; ?>
        </div>
      </div>
      <?php if (isset($_GET["featured"])) { ?>
        <div class="post-pagination post-pagination--alt">
          <!-- paginate through featured posts by menu_order -->
          <p class="post-back-alt">
            <?php echo '<a href="'.$smartNextPost.'">< Previous Project</a>'; ?>
          </p>
          <p class="post-next-alt">
            <?php echo '<span><a href="'.$smartPreviousPost.'">Next Project ></a></span>'; ?>
          </p>
        </div>
      <?php } ?>
    <?php endwhile; endif; ?>
  </div>
</section>


<?php if (isset($_GET["featured"])) { ?>
  <?php
  // TODO: Fix slowness associated with this
  $paged = (get_query_var("page")) ? get_query_var("page") : 1;
  if(1 == $paged) {
    $featuredArgs = array (
      "post_type" => array( "work" ),
      "category_name" => "featured",
      "orderby" => "menu_order",
      "order" => "DESC",
      "numberposts" => -1,
      "posts_per_page" => -1
    );
    $featured_posts = new WP_Query( $featuredArgs );
    if ( $featured_posts->have_posts() ) : ?>
      <section class="work-wrap">
        <div class="work">
          <div class="work-list js-masonry-related">
            <?php while ( $featured_posts->have_posts() ) : $featured_posts->the_post(); ?>
              <?php get_template_part("partials/work", "item"); ?>
            <?php endwhile; ?>
          </div>
        </div>
      </section>
    <?php endif;
  } ?>
<?php } else {
  get_template_part("sections/related");
} ?>

<?php get_footer(); ?>
