<?php get_header(); ?>

<section class="post-single-wrap">
  <div class="post-single">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="post-single-left">
        <div class="post-content">
          <?php
            $image_id = get_post_thumbnail_id();
            $image_low_res = wp_get_attachment_image_src($image_id,'post_lowres', true);
            $image_post = wp_get_attachment_image_src($image_id,'post', true);
            $image_post_2x = wp_get_attachment_image_src($image_id,'post_2x', true);
          ?>
          <!--[if lte IE 9 ]>
          <img
            src="<?php echo $image_post[0]; ?>"
          >
          <![endif]-->
          <!--[if gt IE 9]><!-->
          <img
            src="<?php echo $image_low_res[0]; ?>"
            data-src="<?php echo $image_post[0]; ?>"
            data-src-retina="<?php echo $image_post_2x[0]; ?>"
          >
          <!--<![endif]-->
          <?php $image_id = get_post_thumbnail_id(); ?>
          <div class="post-meta">
            <h1><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h1>
            <p class="post-date"><?php the_date("F jS, Y"); ?></p>
          </div>
          <?php the_content(); ?>
          <?php edit_post_link("Edit"); ?>
        </div>
      </div>
      <?php hs_single_navigation("Post"); ?>
    <?php endwhile; endif; ?>
  </div>
</section>

<?php get_footer(); ?>
