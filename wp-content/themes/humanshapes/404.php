<?php get_header(); ?>

<section class="post-single-wrap">
  <div class="post-single">
    <div class="post-single-left">
      <div class="post-content post-content--full">
        <img class="less-margin" src="<?php echo get_template_directory_uri(); ?>/images/404.gif" data-src="<?php echo get_template_directory_uri(); ?>/images/404.gif" data-src-retina="<?php echo get_template_directory_uri(); ?>/images/4042x.gif">
      </div>
    </div>
  </div>
</section>

<?php get_template_part("sections/footer"); ?>
<?php get_footer(); ?>
