<?php get_header(); ?>

<?php get_template_part("sections/work-tags"); ?>
<section class="work-wrap">
  <div class="work">
    <div class="work-list js-masonry">
      <?php
      $paged = (get_query_var("page")) ? get_query_var("page") : 1;
      if(1 == $paged) {
        $featuredArgs = array (
          "post_type" => array( "work" ),
          "category_name" => "featured",
          "orderby" => "menu_order",
          "order" => "DESC",
          "numberposts" => -1,
          "posts_per_page" => -1
        );
        $featured_posts = new WP_Query( $featuredArgs );
        if ( $featured_posts->have_posts() ) : while ( $featured_posts->have_posts() ) : $featured_posts->the_post();
          get_template_part("partials/work", "item");
        endwhile; endif;
      }
      ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>
