/*global module:false*/
module.exports = function(grunt) {
  require('jit-grunt')(grunt);
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    WPThemeInfo: '/*!\n' +
      'Theme Name: EricNyffeler 2015\n' +
      'Author: Developed by Cody Peterson and Jason Sawyer\n' +
      'Author URI: http://humanshapes.co/\n' +
      'Description: 2015 Website\n' +
      'Version: 3.0\n' +
      'README: This file was generated from the SCSS files within the source directory. Do not edit directly - use the build process.\n' +
      '*/\n',
    favicons: {
      options: {
        appleTouchBackgroundColor: "#fff",
        appleTouchPadding: 30,
        coast: true,
        icoBackgroundColor: "transparent"
      },
      icons: {
        src: 'favicons/source.png',
        dest: '../favicons/'
      }
    },
    modernizr: {
      dist: {
        "devFile" : "remote",
        "outputFile" : "../modernizr-custom.js",
        "extra" : {
          "shiv" : true,
          "printshiv" : false,
          "load" : true,
          "mq" : false,
          "cssclasses" : true
        },
        "extensibility" : {
          "addtest" : false,
          "prefixed" : true,
          "teststyles" : false,
          "testprops" : false,
          "testallprops" : false,
          "hasevents" : false,
          "prefixes" : false,
          "domprefixes" : false
        },
        "uglify" : true,
        "tests" : [
          // http://modernizr.github.io/Modernizr/test/
          "forms_placeholder",
          "rgba",
          "backgroundsize",
          "css-objectfit",
          "flexbox"
        ],
        "parseFiles" : true,
        // When parseFiles = true, this task will crawl all *.js, *.css, *.scss files, except files that are in node_modules/.
        // You can override this by defining a "files" array below.
        // "files" : {
            // "src": []
        // },
        "matchCommunityTests" : true,
        "customTests" : []
      }
    },
    svgmin: {
      files: {
        expand: true,
        cwd: 'svgs',
        src: ['*.svg'],
        dest: 'svgs/minified/',
      }
    },
    grunticon: {
      icons: {
        files: [{
            expand: true,
            cwd: 'svgs/minified/',
            src: ['*.svg', '*.png'],
            dest: "../icons/"
        }],
        options: {
          datasvgcss: "icons.data.svg.css",
          datapngcss: "icons.data.png.css",
          urlpngcss: "icons.fallback.css"
        }
      }
    },
    sass: {
      build: {
        options: {
          style: 'compressed',
          banner: '<%= WPThemeInfo %>',
        },
        files: {
          '../style.min.css': ['css/site.scss'],
        }
      },
      unminified: {
        options: {
          style: 'nested',
          banner: '<%= WPThemeInfo %>',
        },
        files: {
          '../style.css': ['css/site.scss']
        }
      }
    },
    coffee: {
      compile: {
        files: {
          'js/site.js': 'js/site.js.coffee'
        }
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: [
          'bower_components/jquery/dist/jquery.js',
          'bower_components/respond/dest/respond.src.js',
          'bower_components/jquery-validation/dist/jquery.validate.js',
          'bower_components/jquery-placeholder/jquery.placeholder.js',
          'bower_components/sticky-kit/jquery.sticky-kit.min.js',
          'bower_components/handlebars/handlebars.js',
          'bower_components/moment/moment.js',
          'bower_components/Autolinker.js/dist/Autolinker.js',
          'js/waterfall/jquery.waterfall.js',
          'bower_components/imagesloaded/imagesloaded.pkgd.min.js',
          'bower_components/layzrjs/src/layzr.js',
          'js/site.js'
        ],
        dest: '../compiled.js'
      }
    },
    watch: {
      css: {
        options: {
          interrupt: true,
          livereload: true
        },
        files: ['css/**/*.scss'],
        tasks: ['sass']
      },
      js: {
        options: {
          interrupt: true
        },
        files: ['js/*.coffee'],
        tasks: ['js']
      },
      grunt: {
        files: ['Gruntfile.js']
      }
    },
    copy: {
      misc: {
        files: [
          {expand: true, src: ['fonts/**/*'], dest: '../'},
          {expand: true, src: ['icons/**/*'], dest: '../'},
        ]
      },
      favicon: {
        files: [
          {expand: true, cwd: 'favicons/', src: ['favicon.ico'], dest: '../favicons/'},
        ]
      }
    },
    uglify: {
      build: {
        files: {
          '../compiled.min.js' : '../compiled.js'
        }
      }
    },
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'images/',
          src: ['*.{png,jpg,gif}'],
          dest: '../images/'
        }]
      }
    }
  });

  grunt.registerTask('js', ['coffee', 'concat', 'uglify']);
  grunt.registerTask('icons', ['svgmin','grunticon','copy:misc']);
  grunt.registerTask('all', ['favicons', 'modernizr', 'icons', 'copy', 'sass', 'js', 'imagemin']);
  grunt.registerTask('default', ['copy', 'sass', 'js']);
  grunt.registerTask('favicon', ['favicons', 'copy:favicon']);
};
