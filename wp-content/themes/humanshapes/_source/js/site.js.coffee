$(document).on "ready", ->
    EricNyffeler.init()

$(window).load ->

EricNyffeler =
    equalHeight: (group) ->
        tallest = 0
        group
            .height("auto")
            .each ->
                thisHeight = $(this).height()
                if thisHeight > tallest
                    tallest = thisHeight
            .height(tallest)

    sharePopup: (href) ->
        w = 600
        h = 300
        left = (screen.width / 2) - (w / 2)
        top = (screen.height / 2) - (h / 2)
        shareWindow = window.open(
            href
            "EricNyffeler"
            "location=1,status=1,scrollbars=1,
            width=" + w + ",height=" + h + ",top=" + top + ",left=" + left
        )
        return false

    hsFeeds: (container, count) ->
        if $("#feeds-template").length
            template = Handlebars.compile $("#feeds-template").html()
            jqxhr = $.getJSON('https://app.waaffle.com/api/v1.1/campaign/14b3aefa-983b-4ac7-9958-ac18d821c267/posts?key=l2qlYYd4WB7CCDDR8lAL4cIfNrt3RvHa3GZytxk2Y1gsNELkjNe3wAas1zhl', (data) ->
                # console.log data
                $.each data, (i, item) ->
                    # console.log item
                    item.time = moment(item.published_timestamp).format("MMM Do")
                    if item.content
                        # Sometimes posts don't have text but have images. We should just show
                        ## the image. But for now, we aren't showing small images so let's just
                        ## not show anything if there is no text content.
                        item.content_linked = Autolinker.link( item.content )
                        container.append template(item)
                    #Set number of items to show
                    return i < (count - 1)
            ).fail(->
                console.log "Feed Error"
                $('.connect').remove()
                )

    hsFeedsLimit: (container, count) ->
        container.slice(count).hide();

    navToggle: ->
        $(".js-toggle-nav").click ->
            $("body").toggleClass "show-nav"

    fixedNav: ->
        $(".gallery").imagesLoaded ->
            browserWidth = $(window).width()

            if (browserWidth > 840)
                # Enable Sticky Nav
                $(".js-sticky:not(.is-sticky)")
                .stick_in_parent(offset_top: 20)
                .addClass("is-sticky")
                .on("sticky_kit:bottom", (e) ->
                    $(this).parent().css "position", "static"
                    return
                ).on "sticky_kit:unbottom", (e) ->
                    $(this).parent().css "position", "relative"
                    return
            else
                # Disable Sticky Nav (for Mobile)
                $(".js-sticky.is-sticky")
                .trigger("sticky_kit:detach")
                .removeClass("is-sticky")

    initMasonry: ->
        $('.paged .js-masonry-paged, .js-masonry, .js-masonry-related').imagesLoaded ->
            EricNyffeler.masonry()
            if ( !($("html").hasClass("ie9") || $("html").hasClass("lt-ie9")) )
                EricNyffeler.lazyLoad()

    initLazyLoad: ->
        $404 = $(".error404")
        $thanks = $(".page-id-42")
        if $thanks.length or $404.length
            EricNyffeler.lazyLoad()

    lazyLoad: ->
        # Lazy Loading of Images
        layzr = new Layzr (
            container: null
            selector: "[data-src]"
            attr: "data-src"
            retinaAttr: "data-src-retina"
            bgAttr: "data-src-bg"
            hiddenAttr: "data-src-hidden"
            threshold: 50
            callback: ->
                @classList.add "is-loaded"
        )

    masonry: ->
        $work = $(".js-masonry")
        if $work.length
            $work.waterfall
                colMinWidth: 240
                defaultContainerWidth: 1200
                autoresize: false

        $relatedWork = $(".js-masonry-related")
        if $relatedWork.length
            browserWidth = $(window).width()
            $colMinWidth = 240
            if browserWidth < 640
                $colMinWidth = 120
            $relatedWork.waterfall
                colMinWidth: $colMinWidth
                defaultContainerWidth: 1200
                autoresize: false

        $blog = $(".paged .js-masonry-paged") # Only on paged views (not first page)
        if $blog.length
            $blog.waterfall
                colMinWidth: 412
                defaultContainerWidth: 1200
                autoresize: false

    onResize: ->
        EricNyffeler.equalHeight $(".about-content")
        EricNyffeler.fixedNav()

    onLoad: ->
        EricNyffeler.onResize()
        EricNyffeler.navToggle()
        EricNyffeler.hsFeeds $('.js-feed'), 6
        EricNyffeler.initMasonry()
        if ( !($("html").hasClass("ie9") || $("html").hasClass("lt-ie9")) )
            EricNyffeler.initLazyLoad()

    init: ->
        EricNyffeler.onLoad()
        $(window).resize ->
            EricNyffeler.masonry()
            EricNyffeler.onResize()

        $("body").addClass "animate-bg"

        $(".js-share-popup").click (event) ->
            event.preventDefault()
            EricNyffeler.sharePopup $(this).attr "href"

        $("input, textarea").placeholder()

        $(document).ready ->
            $('.header .dropdown-button').click ->
                $subMenu = $('.sub-menu')
                $subMenu.toggleClass 'show-menu'
                $('.sub-menu > li').click ->
                    $subMenu.removeClass 'show-menu'
                    return
                return
            return

        $('.js-contact').submit (e) ->
            e.preventDefault()
            formEl = $(this)
            submitButton = $('input[type=submit]', formEl)
            $.ajax(
                type: 'POST'
                url: formEl.prop('action')
                accept: javascript: 'application/javascript'
                data: formEl.serialize()
                beforeSend: ->
                    submitButton.prop 'disabled', 'disabled'
                    formEl.attr "class", "contact js-contact js-contact-loading"
                success: ->
                    formEl.attr "class", "contact js-contact js-contact-success"
                error: ->
                    formEl.attr "class", "contact js-contact js-contact-error"
            ).done (data) ->
                submitButton.prop 'disabled', false
                return
            return

class EricNyffeler.Controller
