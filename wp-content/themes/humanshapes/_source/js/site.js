(function() {
  var EricNyffeler;

  $(document).on("ready", function() {
    return EricNyffeler.init();
  });

  $(window).load(function() {});

  EricNyffeler = {
    equalHeight: function(group) {
      var tallest;
      tallest = 0;
      return group.height("auto").each(function() {
        var thisHeight;
        thisHeight = $(this).height();
        if (thisHeight > tallest) {
          return tallest = thisHeight;
        }
      }).height(tallest);
    },
    sharePopup: function(href) {
      var h, left, shareWindow, top, w;
      w = 600;
      h = 300;
      left = (screen.width / 2) - (w / 2);
      top = (screen.height / 2) - (h / 2);
      shareWindow = window.open(href, "EricNyffeler", "location=1,status=1,scrollbars=1,            width=" + w + ",height=" + h + ",top=" + top + ",left=" + left);
      return false;
    },
    hsFeeds: function(container, count) {
      var jqxhr, template;
      if ($("#feeds-template").length) {
        template = Handlebars.compile($("#feeds-template").html());
        return jqxhr = $.getJSON('https://app.waaffle.com/api/v1.1/campaign/14b3aefa-983b-4ac7-9958-ac18d821c267/posts?key=l2qlYYd4WB7CCDDR8lAL4cIfNrt3RvHa3GZytxk2Y1gsNELkjNe3wAas1zhl', function(data) {
          return $.each(data, function(i, item) {
            item.time = moment(item.published_timestamp).format("MMM Do");
            if (item.content) {
              item.content_linked = Autolinker.link(item.content);
              container.append(template(item));
            }
            return i < (count - 1);
          });
        }).fail(function() {
          console.log("Feed Error");
          return $('.connect').remove();
        });
      }
    },
    hsFeedsLimit: function(container, count) {
      return container.slice(count).hide();
    },
    navToggle: function() {
      return $(".js-toggle-nav").click(function() {
        return $("body").toggleClass("show-nav");
      });
    },
    fixedNav: function() {
      return $(".gallery").imagesLoaded(function() {
        var browserWidth;
        browserWidth = $(window).width();
        if (browserWidth > 840) {
          return $(".js-sticky:not(.is-sticky)").stick_in_parent({
            offset_top: 20
          }).addClass("is-sticky").on("sticky_kit:bottom", function(e) {
            $(this).parent().css("position", "static");
          }).on("sticky_kit:unbottom", function(e) {
            $(this).parent().css("position", "relative");
          });
        } else {
          return $(".js-sticky.is-sticky").trigger("sticky_kit:detach").removeClass("is-sticky");
        }
      });
    },
    initMasonry: function() {
      return $('.paged .js-masonry-paged, .js-masonry, .js-masonry-related').imagesLoaded(function() {
        EricNyffeler.masonry();
        if (!($("html").hasClass("ie9") || $("html").hasClass("lt-ie9"))) {
          return EricNyffeler.lazyLoad();
        }
      });
    },
    initLazyLoad: function() {
      var $404, $thanks;
      $404 = $(".error404");
      $thanks = $(".page-id-42");
      if ($thanks.length || $404.length) {
        return EricNyffeler.lazyLoad();
      }
    },
    lazyLoad: function() {
      var layzr;
      return layzr = new Layzr({
        container: null,
        selector: "[data-src]",
        attr: "data-src",
        retinaAttr: "data-src-retina",
        bgAttr: "data-src-bg",
        hiddenAttr: "data-src-hidden",
        threshold: 50,
        callback: function() {
          return this.classList.add("is-loaded");
        }
      });
    },
    masonry: function() {
      var $blog, $colMinWidth, $relatedWork, $work, browserWidth;
      $work = $(".js-masonry");
      if ($work.length) {
        $work.waterfall({
          colMinWidth: 240,
          defaultContainerWidth: 1200,
          autoresize: false
        });
      }
      $relatedWork = $(".js-masonry-related");
      if ($relatedWork.length) {
        browserWidth = $(window).width();
        $colMinWidth = 240;
        if (browserWidth < 640) {
          $colMinWidth = 120;
        }
        $relatedWork.waterfall({
          colMinWidth: $colMinWidth,
          defaultContainerWidth: 1200,
          autoresize: false
        });
      }
      $blog = $(".paged .js-masonry-paged");
      if ($blog.length) {
        return $blog.waterfall({
          colMinWidth: 412,
          defaultContainerWidth: 1200,
          autoresize: false
        });
      }
    },
    onResize: function() {
      EricNyffeler.equalHeight($(".about-content"));
      return EricNyffeler.fixedNav();
    },
    onLoad: function() {
      EricNyffeler.onResize();
      EricNyffeler.navToggle();
      EricNyffeler.hsFeeds($('.js-feed'), 6);
      EricNyffeler.initMasonry();
      if (!($("html").hasClass("ie9") || $("html").hasClass("lt-ie9"))) {
        return EricNyffeler.initLazyLoad();
      }
    },
    init: function() {
      EricNyffeler.onLoad();
      $(window).resize(function() {
        EricNyffeler.masonry();
        return EricNyffeler.onResize();
      });
      $("body").addClass("animate-bg");
      $(".js-share-popup").click(function(event) {
        event.preventDefault();
        return EricNyffeler.sharePopup($(this).attr("href"));
      });
      $("input, textarea").placeholder();
      $(document).ready(function() {
        $('.header .dropdown-button').click(function() {
          var $subMenu;
          $subMenu = $('.sub-menu');
          $subMenu.toggleClass('show-menu');
          $('.sub-menu > li').click(function() {
            $subMenu.removeClass('show-menu');
          });
        });
      });
      return $('.js-contact').submit(function(e) {
        var formEl, submitButton;
        e.preventDefault();
        formEl = $(this);
        submitButton = $('input[type=submit]', formEl);
        $.ajax({
          type: 'POST',
          url: formEl.prop('action'),
          accept: {
            javascript: 'application/javascript'
          },
          data: formEl.serialize(),
          beforeSend: function() {
            submitButton.prop('disabled', 'disabled');
            return formEl.attr("class", "contact js-contact js-contact-loading");
          },
          success: function() {
            return formEl.attr("class", "contact js-contact js-contact-success");
          },
          error: function() {
            return formEl.attr("class", "contact js-contact js-contact-error");
          }
        }).done(function(data) {
          submitButton.prop('disabled', false);
        });
      });
    }
  };

  EricNyffeler.Controller = (function() {
    function Controller() {}

    return Controller;

  })();

}).call(this);
