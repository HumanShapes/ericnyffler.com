<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9 ]>        <html class="no-js ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>
      <?php bloginfo("name"); ?>
      <?php wp_title("/"); ?>
      <?php
        $site_description = get_bloginfo( "description", "display" );
        if ( $site_description && ( is_home() || is_front_page() ) )
          echo " / $site_description";
      ?>
    </title>

    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_directory' ); ?>/style.min.css" />
    <?php get_template_part("partials/grunticon"); ?>

    <?php get_template_part("partials/typekit"); ?>

    <script src="<?php bloginfo( 'template_directory' ); ?>/modernizr-custom.js"></script>

    <meta name="viewport" content="width=device-width,initial-scale=1">
    <?php get_template_part("partials/favicons"); ?>

    <meta content="Eric Nyffeler" property="og:site_name">
    <meta content="Eric Nyffeler" property="og:title">
    <meta content="" property="og:description">
    <meta name="description" content="">
    <meta content="http://ericnyffeler.com" property="og:url">
    <meta content="website" property="og:type">
    <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/favicons/share.jpg" />
    <?php wp_head(); ?>
    <?php get_template_part("partials/analytics"); ?>
  </head>
  <body <?php body_class(); ?>>

    <?php get_template_part("sections/header"); ?>
