<?php get_header(); ?>

<section class="post-single-wrap">
  <div class="post-single">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="post-single-left">
        <div class="post-content post-content--full">
          <img class="less-margin" src="<?php echo get_template_directory_uri(); ?>/images/thanks.gif" data-src="<?php echo get_template_directory_uri(); ?>/images/thanks.gif" data-src-retina="<?php echo get_template_directory_uri(); ?>/images/thanks2x.gif">
        </div>
      </div>
    <?php endwhile; endif; ?>
  </div>
</section>

<?php get_template_part("sections/footer"); ?>
<?php get_footer(); ?>
