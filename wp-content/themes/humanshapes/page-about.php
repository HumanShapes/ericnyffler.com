<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<section class="about-wrap">
  <div class="about">
    <?php the_post_thumbnail(); ?>
    <div>
      <?php
      global $more;

      $more = 0;
      echo '<div class="about-half about-left"><div class="about-content">';
      the_content("");
      echo '</div></div>';

      $more = 1;
      echo '<div class="about-half about-right"><div class="about-content">';
      the_content("",true);
      echo '</div></div>';
      ?>
    </div>
  </div>
</section>
<?php endwhile; endif; ?>

<?php get_template_part("sections/footer"); ?>
<?php get_footer(); ?>
