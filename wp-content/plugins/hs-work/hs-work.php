<?php
/*
Plugin Name: HS Work
Description: A custom post type for Indivisible work members by Human Shapes.
Version: 1.0
Author URI: http://humanshapes.co
*/

function hs_work() {
  $labels = array(
    'name'               => _x( 'Work', 'post type general name' ),
    'singular_name'      => _x( 'Work', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'Work' ),
    'add_new_item'       => __( 'Add New Work' ),
    'edit_item'          => __( 'Edit Work' ),
    'new_item'           => __( 'New Work' ),
    'all_items'          => __( 'All Work' ),
    'view_item'          => __( 'View Work' ),
    'search_items'       => __( 'Search Work' ),
    'not_found'          => __( 'No Work found' ),
    'not_found_in_trash' => __( 'No Work found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Work'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Work members',
    'public'        => true,
    'menu_position' => 5,
    'menu_icon'     => '',
    'supports'      => array( 'title', 'thumbnail', 'page-attributes', 'editor' ),
    'has_archive'   => true,
    'rewrite' => array('slug'=>'work'),
    'hierarchical' => false,
    'exclude_from_search' => true,
    'taxonomies' => array('post_tag', 'category')
  );
  register_post_type( 'work', $args );
}
add_action( 'init', 'hs_work' );

add_action( 'add_meta_boxes', 'hs_work_more' );
function hs_work_more() {
  add_meta_box(
    'hs_work_more',
    __( 'Details', 'hs_work' ),
    'hs_work_more_content',
    'work',
    'normal',
    'low'
  );
}

function hs_work_icon(){ ?>
  <style> #adminmenu .menu-icon-work div.wp-menu-image:before { content: "\f110"; } </style>
<?php }
add_action( 'admin_head', 'hs_work_icon' );

add_action( 'save_post', 'hs_work_more_content_save' );
function hs_work_more_content_save( $post_id ) {

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
  return;

  if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
    return;
  }

  if ( isset($_POST['post_type']) && ( 'work' == $_POST['post_type']) ) {
    if ( !current_user_can( 'edit_page', $post_id ) )
    return;
  } else {
    return;
  }
  $hs_color = $_POST['_hs_color'];
  update_post_meta( $post_id, '_hs_color', $hs_color );
  if (isset($_POST['_hs_new'])) {
    $hs_new = $_POST['_hs_new'];
  } else {
    $hs_new = "";
  }
  update_post_meta( $post_id, '_hs_new', $hs_new );
  if (isset($_POST['_hs_spaced'])) {
    $hs_spaced = $_POST['_hs_spaced'];
  } else {
    $hs_spaced = "";
  }
  update_post_meta( $post_id, '_hs_spaced', $hs_spaced );
  $hs_shop_url = $_POST['_hs_shop_url'];
  update_post_meta( $post_id, '_hs_shop_url', $hs_shop_url );
}

function hs_work_more_content( $post ) { ?>
  <p>
    <label for="_hs_color">Hex Color: (for hover states)</label><br />
    <input
      class="widefat"
      type="text"
      name="_hs_color"
      id="_hs_color"
      value="<?php echo esc_attr( get_post_meta( $post->ID, '_hs_color', true ) ); ?>"
      size="30"
     />
  </p>
  <p>
    <label for="_hs_new">New Icon</label><br />
    <input
      class="widefat"
      type="checkbox"
      name="_hs_new"
      id="_hs_new"
      value="new"
      <?php if ( get_post_meta($post->ID, '_hs_new', true) ) { echo 'checked="checked"'; } ?>
     />
  </p>
  <p>
    <label for="_hs_spaced">Spaced Gallery Images</label><br />
    <input
      class="widefat"
      type="checkbox"
      name="_hs_spaced"
      id="_hs_spaced"
      value="spaced"
      <?php if ( get_post_meta($post->ID, '_hs_spaced', true) ) { echo 'checked="checked"'; } ?>
     />
  </p>
  <p>
    <label for="_hs_shop_url">Shop URL:</label><br />
    <input
      class="widefat"
      type="text"
      name="_hs_shop_url"
      id="_hs_shop_url"
      value="<?php echo esc_attr( get_post_meta( $post->ID, '_hs_shop_url', true ) ); ?>"
      size="30"
     />
  </p>
<?php
}
